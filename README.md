# Readme File
This code is a snakemake pipeline for performing RNA-Seq quantification using Salmon. - By Ying and Nolan <3

# Quick Start
example run...

```
nohup snakemake -p -j 6 --use-conda --tibanna \
    --tibanna-config root_ebs_size=32 log_bucket=salk-tm-logs \
    --default-remote-prefix salk-tm-dev/ying_temp/ \
    --default-resources mem_mb=60000 disk_mb=1000000 \
    --configfile configs/soy.sra.json \
        > logs/gwastest.log 2>&1 &

```
     


# Config Options
The following config options are required:

1. reference: the reference transcriptome in fasta format
2. outbase: name to the directory where the output will be placed
3. samples: a dictionary containing the name of each sample as key, and the path to the R1, R2 or long reads or SRA as values.

## Example Configs

short PE reads

```
{
    "samples": {
        "GSM2471308": {
            "r1": "GSM2471308_1.fastq.gz",
            "r2": "GSM2471308_2.fastq.gz"
            },
    "reference": "Gmax.Williams82.HPIv01.cds.fasta",
    "outbase": "Gmax_circ_salmon"
}
```

sra: SRA accession number

```
{
    "samples": {
        "SRR15946939": {
            "sra_short": "SRR15946939"
        }
    },
    "reference": "Gmax.Williams82.HPIv02.cds.fasta",
    "outbase": "Gmax_gwastestsra_salmon"
}
```

ont reads

```
{
    "samples": {
        "SRR15946939": {
            "long": "some.ont_cdna.fastq.gz"
        }
    },
    "reference": "Gmax.Williams82.HPIv02.cds.fasta",
    "outbase": "Gmax_gwastestsra_salmon"
}
```

