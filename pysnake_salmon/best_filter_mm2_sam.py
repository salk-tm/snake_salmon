import sys
import argparse


def filter_sam_cli():
    parser = argparse.ArgumentParser(
        description=(
            "filters sam data from minimap2 to include only the best alignment(s) for each read. "
        )
    )
    parser.add_argument(
        "sam",
        type=lambda s: sys.stdin if(s == "-") else open(s),
        help="path to a sam file or '-' to read from stdin"
    )
    parser.add_argument(
        "-o", "--outpath",
        default=sys.stdout,
        type=lambda f : open(f, 'w'),
        help="output path to write sam to. Defaults to stdout"
    )
    args = parser.parse_args()
    filter_sam(args.sam, args.outpath)


def process_read_group(g, outfile):
    """ given a list of sam entries for a specific read, 
    """
    if(all(line[2] == "*" for line in g)):
        towrite = g
    else:
        scores = [int(e[5:]) for l in g for e in l if(e.startswith('ms:i:'))]
        assert(len(scores) == len(g))
        best = max(scores)
        towrite = [l for s, l in zip(scores, g) if(s == best)]
    for l in towrite:
        outfile.write('\t'.join(l))

    
def filter_sam(sam, outfile):
    group = []
    cur = None
    for line in sam:
        if(line[0] == "@"):
            outfile.write(line)
        else:
            line = line.split("\t")
            if(line[0] != cur):
                if(len(group) > 0):
                    process_read_group(group, outfile)
                group = []
                cur = line[0]
            group.append(line)
    process_read_group(group, outfile)


if(__name__ == "__main__"):
    filter_sam_cli()

