import pandas
from sklearn.decomposition import PCA
from scipy.stats import zscore
from plotly import express
import argparse


def draw_scatter(df):
    fig = express.scatter(
        df,
        x="dim1",
        y="dim2",
        color="labels",
        labels={"labels": "Sample"}
    )
    fig.update_layout(showlegend=True)
    return fig


def stdnorm(df):
    """ Applies zscore to the rows (genes) of df
    """
    ret = df.transpose().apply(zscore).transpose()
    ret = ret.dropna()
    return ret


def dim_reduce(df, reducer=PCA):
    reducer = reducer(n_components=2)
    reduced = pandas.DataFrame(
        reducer.fit_transform(df.transpose()),
        columns=['dim1', 'dim2'],
    )
    reduced["labels"] = df.columns
    return reduced


def main(counts, outpath):
    df = pandas.read_csv(counts, sep="\t")
    tpm = df.set_index(["Name", "Sample"]).TPM.unstack()
    tpm = stdnorm(tpm)
    tpm_2dim = dim_reduce(tpm)
    fig = draw_scatter(tpm_2dim)
    fig.write_html(outpath)


def climain():
    parser = argparse.ArgumentParser(
        description=(
            "generates a pca plot from a counts table"
        )
    )
    parser.add_argument(
        "counts",
        help="path to a counts table"
    )
    parser.add_argument(
        "-o", "--outpath",
        help="output path"
    )
    args = parser.parse_args()
    main(args.counts, args.outpath)
    

def snakemain():
    counts = snakemake.input[0]
    outpath = snakemake.output[0]
    main(counts, outpath)


if(__name__ == "__main__"):
    snakemain()
