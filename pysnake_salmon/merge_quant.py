import pandas
import os
import argparse


quant_cols = "Sample Name Length EffectiveLength TPM NumReads".split()


def load_quant(f):
    df = pandas.read_csv(f, sep="\t")
    df["Sample"] = os.path.basename(os.path.dirname(f))
    return df[quant_cols]


def main(quants, outpath):
    with open(outpath, "w") as fout:
        fout.write('\t'.join(quant_cols)+ "\n")
        for qf in quants:
            load_quant(qf).to_csv(fout, sep="\t", header=None, index=None)


def climain():
    parser = argparse.ArgumentParser(
        description=(
            "Merges a set of quant.sf files into a single table"
        )
    )
    parser.add_argument(
        "quants",
        nargs="+",
        help="path to a quantsf file as output by salmon"
    )
    parser.add_argument(
        "-o", "--outpath",
        help="output path"
    )
    args = parser.parse_args()
    main(args.quants, args.outpath)
    

def snakemain():
    quant_files = snakemake.input
    outpath = snakemake.output[0]
    main(quant_files, outpath)


if(__name__ == "__main__"):
    snakemake()