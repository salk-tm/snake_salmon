import pandas
import json
import os
import argparse


# species which keys are going to be extracted from the json files
# to be merged into a stats table
lib_keys = [
    "expected_format",
    "compatible_fragment_ratio",
    "num_compatible_fragments",
    "num_assigned_fragments",
    "num_frags_with_concordant_consistent_mappings",
    "num_frags_with_inconsistent_or_orphan_mappings",
    "strand_mapping_bias",
    "MSF", "OSF", "ISF", "MSR", "OSR", "ISR",
    "SF", "SR", "MU", "OU", "IU", "U"
]

meta_keys = [
    "num_bootstraps",
    "num_processed",
    "num_mapped",
    "num_decoy_fragments",
    "num_dovetail_fragments",
    "num_fragments_filtered_vm",
    "num_alignments_below_threshold_for_mapped_fragments_vm",
    "percent_mapped",
    "frag_dist_length",
    "frag_length_mean",
    "frag_length_sd",
    "seq_bias_correct",
    "gc_bias_correct",
    "num_bias_bins",
    "mapping_type",
    "keep_duplicates",
    "num_valid_targets",
    "num_decoy_targets",
    "num_eq_classes",
]


def load_stats(lib, meta):
    with open(lib) as fin:
        lib = json.load(fin)
    with open(meta) as fin:
        meta = json.load(fin)
    ret = {}
    for k in lib_keys:
        ret[k] = lib.get(k)
    for k in meta_keys:
        ret[k] = meta.get(k)
    return ret    


def main(lib_files, meta_files, outpath):
    df = pandas.DataFrame({
        os.path.basename(os.path.dirname(l)) : load_stats(l, m)
        for l, m in zip(lib_files, meta_files)
    })
    df.columns.name = "Sample"
    df.transpose().iloc[:, 1:].to_csv(outpath, sep="\t")


def climain():
    parser = argparse.ArgumentParser(
        description=(
            "Merges a set of json files from salmon into a single table"
        )
    )
    parser.add_argument(
        "-m", "--meta",
        nargs="+",
        help="path to a meta_info.json files as output by salmon"
    )
    parser.add_argument(
        "-l", "--lib",
        nargs="+",
        help="path to lib_format_counts.json as output by salmon"
    )
    parser.add_argument(
        "-o", "--outpath",
        help="output path"
    )
    args = parser.parse_args()
    main(args.lib, args.meta, args.outpath)
    


def snakemain():
    lib_files = snakemake.input["lib_stats"]
    meta_files = snakemake.input["meta_stats"]
    outpath = snakemake.output[0]
    main(lib_files, meta_files, outpath)


if(__name__ == "__main__"):
    snakemain()