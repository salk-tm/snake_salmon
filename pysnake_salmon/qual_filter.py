import sys
from collections import Counter
import pandas
import argparse


def filter_fastq_cli():
    parser = argparse.ArgumentParser(
        description=(
            "filters fastq data by average quality score. "
        )
    )
    parser.add_argument(
        "-f", "--fastq",
        type=lambda s: sys.stdin if(s == "-") else open(s)
        help="path to a fastq file or '-' to read from stdin"
    )
    parser.add_argument(
        "-c", "--cutoff",
        type=float,
        default=13
        help="A minimum mean quality a read must have to pass filtering"
    )
    parser.add_argument(
        "-o", "--outpath",
        default=sys.stdout
        type= lambda f : open(f, 'w')
        help="output path to write fastq to. Defaults to stdout"
    )
    args = parser.parse_args()
    filter_fastq(args.fastq, args.cutoff, args.outpath)

    
def filter_fastq(fastq, cutoff, outfile):
    for entry in zip(fastq, fastq, fastq, fastq):
        scores = [ord(c) - 33 for c in entry[3][:-1]]
        qual_counts += Counter(scores)
        mean_score = sum(scores) / len(scores)
        if(mean_score >= cutoff):
            for line in entry:
                outfile.write(line)


if(__name__ == "__main__"):
    filter_fastq_cli()

