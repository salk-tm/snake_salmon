import os
from snakemake.utils import validate


validate(config, "schema/config.schema.json")
# assert(all('r1' in s and 'r2' in s or 'long' in s for s in config['samples'].values()))
config['outbase'] = config['outbase'].rstrip("/")

salmon_idx = config["outbase"] + "/salmon_idx/"

salmon_index_files = """complete_ref_lens.bin
ctable.bin
ctg_offsets.bin
duplicate_clusters.tsv
info.json
mphf.bin
pos.bin
pre_indexing.log
rank.bin
refAccumLengths.bin
ref_indexing.log
reflengths.bin
refseq.bin
seq.bin
versionInfo.json""".split()

final_files = ["quant.tsv", "lib_stats.tsv", "pca_plots.html"]
final_files = [config['outbase'] + "/" + s for s in final_files]
groupbase = os.path.basename(config['outbase'])


rule all:
    input:
        final_files

rule bam_to_fastq:
    input:
        "{base}.bam"
    output:
        "{base}.hifi.fastq.gz"
    conda:
        "envs/general.yml"
    threads:
        16
    group:
        groupbase + "_bam_to_fastq"
    shell:
        """
        samtools fastq -@ {threads} {input} | bgzip -@ {threads} > {wildcards.base}.fastq.gz
        fastq-filter -q 20 -o {output} {wildcards.base}.fastq.gz
        """


rule index:
    input:
        config["reference"]
    output:
        [f'{salmon_idx}{f}' for f in salmon_index_files]
    conda:
        "envs/general.yml"
    params:
        opt = config["opts"]["index"],
        idx = salmon_idx
    group:
        groupbase + "_idx"
    shell:
        """
        salmon index {params.opt} -t {input} -i {params.idx}
        tree
        """


rule quant_sr:
    input:
        index_files = rules.index.output,
        r1 = lambda wcs: config['samples'][wcs['samp']]['r1'],
        r2 = lambda wcs: config['samples'][wcs['samp']]['r2']
    output:
        lib_info = config['outbase'] + "/{samp}.sr/lib_format_counts.json",
        quant = config['outbase'] + "/{samp}.sr/quant.sf",
        meta = config['outbase'] + "/{samp}.sr/meta_info.json"
    conda:
        "envs/general.yml"
    params:
        idxdir = salmon_idx,
        opt = config["opts"]["quant"],
        outdir = lambda wcs: f"{config['outbase']}/{wcs['samp']}.sr/"
    group:
        lambda wcs: groupbase + "_quant_" + wcs["samp"]
    threads:
        8
    shell:
        """
        salmon quant {params.opt} -p {threads} -i {params.idxdir} -o {params.outdir} -1 {input.r1} -2 {input.r2}
        mv {params.outdir}aux_info/meta_info.json {output[2]}
        """


rule quant_single_short:
    input:
        index_files = rules.index.output,
        r1 = lambda wcs: config['samples'][wcs['samp']]['single']
    output:
        lib_info = config['outbase'] + "/{samp}.single/lib_format_counts.json",
        quant = config['outbase'] + "/{samp}.single/quant.sf",
        meta = config['outbase'] + "/{samp}.single/meta_info.json"
    conda:
        "envs/general.yml"
    params:
        idxdir = salmon_idx,
        opt = config["opts"]["quant"],
        outdir = lambda wcs: f"{config['outbase']}/{wcs['samp']}.single/"
    group:
        lambda wcs: groupbase + "_quant_" + wcs["samp"]
    threads:
        8
    shell:
        """
        salmon quant {params.opt} -p {threads} -i {params.idxdir} -o {params.outdir} -r {input.r1}
        mv {params.outdir}aux_info/meta_info.json {output[2]}
        """


rule quant_lr:
    input:
        ref = config['reference'],
        reads = lambda wcs: config['samples'][wcs['samp']]['long']
    output:
        lib_info = config['outbase'] + "/{samp}.long/lib_format_counts.json",
        quant = config['outbase'] + "/{samp}.long/quant.sf",
        meta = config['outbase'] + "/{samp}.long/meta_info.json"
    params:
        opt = config["opts"]["quant"],
        outdir = lambda wcs: f"{config['outbase']}/{wcs['samp']}.long/",
        opt_mm2 = config['opts']['mm2'],
        filter_opt = "| sn_best_filter_mm2_sam -" if config['best_filter'] else ""
    conda:
        "envs/general.yml"
    group:
        lambda wcs: groupbase + "_quant_" + wcs["samp"]
    threads:
        16
    shell:
        """
        minimap2 -ax splice {params.opt_mm2} -t {threads} {input.ref} {input.reads} {params.filter_opt} | samtools view -b > {wildcards.samp}.bam
        salmon quant --ont --noLengthCorrection -t {input.ref} {params.opt} -a {wildcards.samp}.bam -o {params.outdir}
        mv {params.outdir}aux_info/meta_info.json {output.meta}
        echo '{{}}' > {output.lib_info}
        tree
        """
    
def quant_sra_short_input(wcs):
	test = config['samples'][wcs['samp']]["sra_short"]
	return rules.index.output
	
    
rule quant_srashort:
    input:
        index_files = quant_sra_short_input
    output:
        lib_info = config['outbase'] + "/{samp}.sra_sr/lib_format_counts.json",
        quant = config['outbase'] + "/{samp}.sra_sr/quant.sf",
        meta = config['outbase'] + "/{samp}.sra_sr/meta_info.json"
    conda:
        "envs/general.yml"
    params:
        idxdir = salmon_idx,
        opt = config["opts"]["quant"],
        outdir = lambda wcs: f"{config['outbase']}/{wcs['samp']}.sra_sr/",
    	sra = lambda wcs: config['samples'][wcs['samp']]['sra_short']
    group:
        lambda wcs: groupbase + "_quant_" + wcs["samp"]
    threads:
        8
    shell:
        """
        fasterq-dump {params.sra} 
        if test -f {params.sra}.fastq
        then
        	salmon quant {params.opt} -p {threads} -i {params.idxdir} -o {params.outdir} -r {params.sra}.fastq
        else
        	salmon quant {params.opt} -p {threads} -i {params.idxdir} -o {params.outdir} -1 {params.sra}_1.fastq -2 {params.sra}_2.fastq
        fi
        mv {params.outdir}aux_info/meta_info.json {output[2]}
        """


def samp_to_outdir(samp):
    d = config['samples'][samp]
    if("long" in d):
        return f"{config['outbase']}/{samp}.long"
    elif("sra_short" in d):
        return f"{config['outbase']}/{samp}.sra_sr"
    elif("single" in d):
        return f"{config['outbase']}/{samp}.single"
    elif("r1" in d and "r2" in d):
        return f"{config['outbase']}/{samp}.sr"
    else:
        raise ValueError(f"unable to process {samp}: {str(d)}")


rule merge:
    input:
        quants = [f"{samp_to_outdir(samp)}/quant.sf" for samp in config["samples"]],
        lib_stats = [f"{samp_to_outdir(samp)}/lib_format_counts.json" for samp in config["samples"]],
        meta_stats = [f"{samp_to_outdir(samp)}/meta_info.json" for samp in config["samples"]]
    output:
        quant = config["outbase"] + "/quant.tsv",
        stats = config["outbase"] + "/lib_stats.tsv"
    params:
        samps = [s for s in config["samples"]],
        outbase = config["outbase"]
    conda:
        "envs/general.yml"
    shell:
        """
        sn_salmon_merge_quant {params.outbase}/*/quant.sf -o {output.quant}
        sn_salmon_merge_stats -m {params.outbase}/*/meta_info.json -l {params.outbase}/*/lib_format_counts.json -o {output.stats}
        """


rule visualize:
    input:
        rules.merge.output.quant
    output:
        config['outbase'] + "/pca_plots.html"
    conda:
        "envs/general.yml"
    shell:
        "sn_salmon_vis {input} -o {output}"

