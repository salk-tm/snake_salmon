from setuptools import setup
import os


setup(
    name="pysnake_salmon",
    version="0.1.1",
    packages=["pysnake_salmon"],
    python_requires=">=3.6",
    install_requires=[
        "scikit-learn",
        "statsmodels",
        "pandas",
        "plotly",
    ],
    entry_points={
        "console_scripts": [
            "sn_salmon_merge_quant = pysnake_salmon.merge_quant:climain",
            "sn_salmon_merge_stats = pysnake_salmon.merge_stats:climain",
            "sn_salmon_vis = pysnake_salmon.vis:climain",
            "sn_best_filter_mm2_sam = pysnake_salmon.best_filter_mm2_sam:filter_sam_cli"
        ]
    }
)
